import logo from './logo.svg';
import './App.css';
import { BrowserRouter,Routes,Route } from 'react-router-dom';
import Services from './components/Services';

import Formulaire from './components/Formulaire';
import NoPage from './components/NoPage';
import Header from './components/Header';
import Acceuil from './components/Acceuil';

function App() {
  return (
    <div className="App">
     
    <BrowserRouter>
      <Routes>
        <Route path="/" element={<Header />}>
          <Route index element={<Acceuil />} />
          <Route path="Formulaire" element={<Formulaire />} />
          
          <Route path="Services" element={<Services />} />
          <Route path="*" element={<NoPage />} />
        </Route>
      </Routes>
    </BrowserRouter>
    </div>
  );
}

export default App;
