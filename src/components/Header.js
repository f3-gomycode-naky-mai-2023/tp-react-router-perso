import React from 'react';
import Button from 'react-bootstrap/Button';
import Container from 'react-bootstrap/Container';
import Form from 'react-bootstrap/Form';
import Nav from 'react-bootstrap/Nav';
import Navbar from 'react-bootstrap/Navbar';
import NavDropdown from 'react-bootstrap/NavDropdown';
import { Outlet, Link } from "react-router-dom";


const Header = () => {
    return (
        <div>
          
          <Navbar expand="lg" className="bg-body-tertiary" bg="secondary" data-bs-theme="dark">
      <Container fluid>
        <Navbar.Brand   href="#">NAKY TECH</Navbar.Brand>
        <Navbar.Toggle aria-controls="navbarScroll" />
        <Navbar.Collapse id="navbarScroll">
          <Nav
            className="me-auto my-2 my-lg-0"
            style={{ maxHeight: '100px' }}
            navbarScrollA
          >
            <Nav.Link ><Link to="/" >Acceuil</Link></Nav.Link>
            <Nav.Link><Link to="/contact" >Contact</Link></Nav.Link>
            <Nav.Link><Link to="/services" >Services</Link></Nav.Link>
            <Nav.Link><Link to="/formulaire" > Formulaire</Link></Nav.Link>
            
            <NavDropdown title="Link" id="navbarScrollingDropdown">
              <NavDropdown.Item ><Link to="/Connexion" >Connexion</Link></NavDropdown.Item>
              <NavDropdown.Item ><Link to="/boutique" >
                Boutique</Link>
              </NavDropdown.Item>
              <NavDropdown.Divider />
              <NavDropdown.Item ><Link to="/commerce" >
                Commerce</Link>
              </NavDropdown.Item>
            </NavDropdown>
            <Nav.Link href="#" disabled>
              Personnels
            </Nav.Link>
          </Nav>
          <Form className="d-flex">
            <Form.Control
              type="search"
              placeholder="Recherche"
              className="me-2"
              aria-label="Search"
            />
            <Button variant="outline-success">Recherche</Button>
          </Form>
        </Navbar.Collapse>
      </Container>
    </Navbar>
    <Outlet />
        </div>
    );
}

export default Header;
