import React from 'react';
import Table from 'react-bootstrap/Table';
import { useNavigate } from 'react-router-dom';
import { UseSelector, useSelector } from 'react-redux/es/hooks/useSelector';

const Contact = () => {
    
  const navigate = useNavigate();
  const users = useSelector((state)=> state.user.users)
  console.log(users)

  function submit(e, id){
    e.preventDefault();
    navigate('/Services'+id)
}
    return (
        <div>
            <Table striped>
      <thead>
        <tr>
          <th>#</th>
          <th>First Name</th>
          <th>Last Name</th>
          <th>email</th>
        </tr>
      </thead>
      <tbody>
        {users && users.length && users.map((user, key)=>(
            <tr key={key} onClick={(e)=>submit(e, user.id)}>
            <td>{user.id}</td>
            <td>{user.prenom}</td>
            <td>{user.nom}</td>
            <td>{user.email}</td>
          </tr>
        ))}
      </tbody>
    </Table>
        </div>
    );
}

export default Contact;
