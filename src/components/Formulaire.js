import React from 'react';
import { useState } from 'react';
import Button from 'react-bootstrap/Button';
import Col from 'react-bootstrap/Col';
import Form from 'react-bootstrap/Form';
import Row from 'react-bootstrap/Row';
import { useNavigate } from 'react-router-dom';
import {  useDispatch } from 'react-redux';
import { addUser } from '../reduxs/reducers/user/userSlice';


const Formulaire = () => {
    const [validated, setValidated] = useState(false);
    const [nom, setNom] = useState("");
    const [prenom, setPrenom] = useState("");
    const [email, setEmail] = useState("");
    const navigate = useNavigate();
    const dispatch = useDispatch();

  const handleSubmit = (event) => {
    const form = event.currentTarget;
    if (form.checkValidity() === false) {
      event.preventDefault();
      event.stopPropagation();
    }
    else{
      const id = (new Date()).toISOString()
      dispatch(addUser({id, nom, prenom, email}))
      navigate('/services')
    }

    setValidated(true);
  };

    
    return (
        <div>
             <Form noValidate validated={validated} onSubmit={handleSubmit}>
                <Row className="mt-5">
                <Form.Group as={Col} md="4" controlId="validationCustom01">
                
                <Form.Control
                required
                type="text"
                placeholder="veuillez entrée votre nom svp"
                defaultValue={nom} 
                onChange ={(e)=>setNom(e.target.value)}
                />
                <Form.Control.Feedback>Looks good!</Form.Control.Feedback>
            </Form.Group>
            <Form.Group as={Col} md="4" controlId="validationCustom02">
                
                <Form.Control
                required
                type="text"
                placeholder="veuillez entrée votre prenom svp"
                defaultValue={prenom}
                onChange={(e)=>setPrenom(e.target.value)}
                />
                <Form.Control.Feedback>Looks good!</Form.Control.Feedback>
            </Form.Group>
            <Form.Group as={Col} md="4" controlId="validationCustom03">
            
            <Form.Control type="text" placeholder="veuillez entrer votre email svp" required 
            defaultValue={email} onChange={(e)=>setEmail(e.target.value)} />
            <Form.Control.Feedback type="invalid">
            veuillez entrer un email valide svp!!!
            </Form.Control.Feedback>
         </Form.Group>
         
         <Button type="submit" className="mt-5">envoyer</Button>
        </Row>

            {/*<Button type="button" onClick={(e)=>handleDelete(current.id)}>supprimer</Button>*/}
        </Form>
        </div>
    );
}

export default Formulaire;
