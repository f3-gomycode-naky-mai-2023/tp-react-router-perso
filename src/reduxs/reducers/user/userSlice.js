import { createSlice } from '@reduxjs/toolkit'

export const userSlice = createSlice({
    name: 'user',
    initialState: {
        users: [],
        user: {},
    },
    reducers: {
        addUser: (state, action) => {
            state.users.push(action.payload)
        },
        editUser: (state, action) => {
            state.user = action.payload
        }
    },
})

// Action creators are generated for each case reducer function
export const { addUser, editUser, } = userSlice.actions

export default userSlice.reducer